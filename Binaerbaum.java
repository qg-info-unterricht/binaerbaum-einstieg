
/**
 * Ein Knoten eines Binärbaums
 * 
 * @author Rainer Helfrich
 * @author Frank Schiebel
 * @version April 2024
 */
public class Binaerbaum<T>
{
    /**
     * Der Datenwert des Knotens
     */
    private T daten;
    
    /**
     * Der linke Kindbaum
     */
    private Binaerbaum<T> links;
    
    /**
     * Der rechte Kindbaum
     */
    private Binaerbaum<T> rechts;
    
    // Der Konstruktor ist überladen, 
    // man kann ihn auf drei Arten aufrufen
    
    /**
     * Erzeugt einen Blattknoten mit leerem Datenwert
     */
    public Binaerbaum()
    {
        this(null, null, null);
    }
    
    /**
     * Erzeugt einen Knoten mit Datenwert und Kindern
     * @param daten Der Datenwert
     * @param links Der linke Kindbaum
     * @param rechts Der rechte Kindbaum
     */
    public Binaerbaum(T daten, Binaerbaum<T> links, Binaerbaum<T> rechts)
    {
        this.daten = daten;
        this.links = links;
        this.rechts = rechts;
    }

    /**
     * Erzeugt einen Blattknoten mit Datenwert
     * @param daten Der Datenwert
     */
    public Binaerbaum(T daten)
    {
        this(daten, null, null);
    }
    
    /**
     * Gibt zurück, ob der Knoten ein Blatt ist
     * @return true, wenn der Knoten ein Blatt ist; false sonst
     */
    public boolean isBlatt()
    {
        return links == null && rechts == null;
    }
    
    /**
     * Gibt den Datenwert zurück
     * @return daten - Das Datenobjekt
     */
    public T getDaten() {
        return this.daten;
    }
    
    /**
     * Setzt as daten-Attribut
     * @param daten Der Datenwert
     */
    public void setDaten(T daten) {
        this.daten = daten;   
    }
    
    /**
     * Setzt den linken Teil-Baum
     * @param Binaerbaum links
     */
    public void setLinks(Binaerbaum b) {
        this.links = b;
    }
    
    /**
     * Setzt den rechten Teil-Baum
     * @param Binaerbaum links
     */
    public void setRechts(Binaerbaum b) {
        this.rechts = b;
    }
    
    /**
     * Holt den linken Teil-Baum
     * @return Binaerbaum links
     */
    public Binaerbaum getLinks() {
        return this.links;
    }
    
    /**
     * Holt den rechten Teil-Baum
     * @return Binaerbaum rechts
     */
    public Binaerbaum getRechts() {
        return this.rechts;
    }
    
}
